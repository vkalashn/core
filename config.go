package core

import (
	"github.com/kelseyhightower/envconfig"
	"log"
	"time"
)

var libConfig = getConfig()

func getConfig() Config {
	conf := Config{}
	err := envconfig.Process("PLUGIN", &conf)
	if err != nil {
		log.Fatal(err)
	}
	return conf
}

type Config struct {
	LogLevel string `envconfig:"PLUGIN_LOGLEVEL" default:"info"`
	IsDev    bool   `envconfig:"PLUGIN_ISDEV" default:"true"`
	Name     string `envconfig:"PLUGIN_NAME"`
	Tenant   string `envconfig:"PLUGIN_TENANT"`
	KeyCloak struct {
		Url       string        `envconfig:"PLUGIN_KEYCLOAK_URL"`
		Login     string        `envconfig:"PLUGIN_KEYCLOAK_LOGIN"`
		Password  string        `envconfig:"PLUGIN_KEYCLOAK_PASSWORD"`
		RealmName string        `envconfig:"PLUGIN_KEYCLOAK_REALMNAME"`
		TokenTTL  time.Duration `envconfig:"PLUGIN_KEYCLOAK_TOKENTTL" default:"250ns"`
	}
	Policy struct {
		Url              string `envconfig:"PLUGIN_POLICY_URL"`
		Repository       string `envconfig:"PLUGIN_POLICY_REPOSITORY"`
		Group            string `envconfig:"PLUGIN_POLICY_GROUP"`
		ExpiresInSeconds int    `envconfig:"PLUGIN_POLICY_TTL"`
	}
	Nats struct {
		Url        string `envconfig:"PLUGIN_NATS_URL"`
		QueueGroup string `envconfig:"PLUGIN_NATS_QUEUEGROUP"`
	}
	Crypto struct {
		Namespace string `envconfig:"PLUGIN_CRYPTO_NAMESPACE"`
	}
}

func SetLibConfig(c Config) {
	libConfig = c
}
