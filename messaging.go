package core

import (
	"context"
	"encoding/json"
	"github.com/google/uuid"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	messaging "gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging/common"
	"strings"
)

const messageErr = "message error"

func NewMessage() (Message, error) {
	client, err := cloudEventsConnection(SignerTopic, cloudeventprovider.ConnectionTypeReq)
	if err != nil {
		return nil, SdkError{general: messageErr, specific: err}
	}
	m := message{client: client}
	return &m, nil
}

type message struct {
	client *cloudeventprovider.CloudEventProviderClient
}

func (m *message) CreateKey(keyId string, accountId string, keyType string) error {
	eventData := createKeyRequest{
		Request: messaging.Request{
			TenantId:  libConfig.Tenant,
			RequestId: strings.Join([]string{libConfig.Name, uuid.New().String()}, "_"),
		},
		Namespace: libConfig.Crypto.Namespace,
		Group:     accountId,
		Key:       keyId,
		Type:      keyType,
	}
	data, err := json.Marshal(eventData)
	if err != nil {
		return SdkError{general: messageErr, specific: err}
	}
	ev, err := cloudeventprovider.NewEvent(libConfig.Name, string(CreateKeyEventType), data)
	if err != nil {
		return SdkError{general: messageErr, specific: err}
	}
	_, err = m.client.RequestCtx(context.Background(), ev)
	if err != nil {
		return SdkError{general: messageErr, specific: err}
	}
	return nil
}
func (m *message) CreateToken(keyId string, accountId string, data []byte) ([]byte, error) {
	eventData := createTokenRequest{
		Request: messaging.Request{
			TenantId:  libConfig.Tenant,
			RequestId: strings.Join([]string{libConfig.Name, uuid.New().String()}, "_"),
		},
		Namespace: libConfig.Tenant,
		Group:     accountId,
		Key:       keyId,
		Payload:   data,
		Header:    make([]byte, 0),
	}
	data, err := json.Marshal(eventData)
	if err != nil {
		return nil, SdkError{general: messageErr, specific: err}
	}
	ev, err := cloudeventprovider.NewEvent(libConfig.Name, string(SignTokenEventType), data)
	if err != nil {
		return nil, SdkError{general: messageErr, specific: err}
	}
	res, err := m.client.RequestCtx(context.Background(), ev)
	var output createTokenReply
	err = res.DataAs(&output)
	if err != nil {
		return nil, SdkError{general: messageErr, specific: err}
	}
	return output.Token, nil
}
