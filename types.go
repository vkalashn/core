package core

import (
	"context"

	"github.com/Nerzal/gocloak/v13"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging/common"
	"net/http"
)

const UserKey = "user"

type EventType string

const (
	CreateKeyEventType EventType = "signer.createKey"
	SignTokenEventType EventType = "signer.signToken"
)

const SignerTopic = "signer-topic"

const (
	UserNotFound = "user not found"
)

type dataFetcher interface {
	GetUserInfo(ctx context.Context, accessToken string, realm string) (*gocloak.UserInfo, error)
}

type UserInfo struct {
	*gocloak.UserInfo
}

func (u *UserInfo) ID() string {
	sub := u.UserInfo.Sub
	return *sub
}

type Message interface {
	CreateKey(string, string, string) error
	CreateToken(string, string, []byte) ([]byte, error)
}

type createKeyRequest struct {
	common.Request
	Namespace string `json:"namespace"`
	Group     string `json:"group"`
	Key       string `json:"key"`
	Type      string `json:"type"`
}

type createTokenRequest struct {
	common.Request
	Namespace string `json:"namespace"`
	Group     string `json:"group"`
	Key       string `json:"key"`
	Payload   []byte `json:"payload"`
	Header    []byte `json:"header"`
}

type createTokenReply struct {
	common.Reply
	Token []byte `json:"token"`
}

type Metadata struct {
	Name        string `json:"name"`
	ID          string `json:"id"`
	Description string `json:"description"`
}
